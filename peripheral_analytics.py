#! /usr/bin/python3
# utf-8
# created by IndiePandaaaaa|Lukas
# inspired by https://thepythoncode.com/article/write-a-keylogger-python

import argparse
import json
import os.path
from datetime import datetime
from threading import Timer

import keyboard
import mouse

FILE_NAME = "peripheralstatistics.json"
INTERVAL = 10  # secs between the log is saved


class PeripheralAnalytics:
    TIMES = "timesPressed"
    DELAY = "totalDelayPerKey"
    STARTER = "starterForTypingSequenceTimes"
    TOTAL = "totalKeyPresses"

    def __init__(self, interval: int, path: str, break_time: float = 5):
        self.file_path = os.path.join(path, FILE_NAME)
        if os.path.isfile(self.file_path):
            with open(self.file_path, "r") as f:
                self.log = json.loads(f.read())
        else:
            # dict(key=dict(times=0, delay=0, starter=0))
            self.log = {self.TOTAL: self.__to_dict(0, 0, 0)}

        self.timestamp_last_input = self.get_timestamp()
        self.report_interval = interval
        self.average_break_time = break_time
        self.total_keyboard_presses = 0

        if self.TOTAL in list(self.log.keys()) and self.TIMES in list(self.log.get(self.TOTAL).keys()) and isinstance(
                self.log.get(self.TOTAL)[self.TIMES], int):
            self.total_keyboard_presses = self.log.get(self.TOTAL)[self.TIMES]

    def __to_dict(self, times: int, delay: float, starter: int):
        return {self.TIMES: times, self.DELAY: delay, self.STARTER: starter}

    def add_to_log(self, name):
        entry = self.log.get(name)
        starter = 0
        delay = self.get_timestamp() - self.timestamp_last_input

        print(f"{format(self.get_timestamp(), '.3f')} - added value [{name}] {delay}s")

        if delay > self.average_break_time:  # detects if there was a longer break for thinking
            delay = self.average_break_time
            starter = 1

        if isinstance(entry, dict) and self.TIMES in list(entry.keys()) and self.DELAY in list(entry.keys()):
            self.log[name] = self.__to_dict(entry.get(self.TIMES) + 1,
                                            entry.get(self.DELAY) + delay,
                                            entry.get(self.STARTER) + starter)
        else:
            self.log[name] = self.__to_dict(1, delay, starter)

        self.timestamp_last_input = self.get_timestamp()

    @staticmethod
    def get_timestamp() -> float:
        return float(datetime.now().strftime("%s.%f"))

    def callback_keyboard(self, event):
        self.add_to_log(f"keyboard_{str(event.name).replace(' ', '_')}")
        self.total_keyboard_presses += 1

    def callback_mouse_right(self):
        self.add_to_log("mouse_right")

    def callback_mouse_middle(self):
        self.add_to_log("mouse_middle")

    def callback_mouse_left(self):
        self.add_to_log("mouse_left")

    def report(self):
        self.log[self.TOTAL] = self.__to_dict(self.total_keyboard_presses, self.average_break_time, 0)
        self.write_json_to_file(self.file_path, self.log)

        timer = Timer(interval=self.report_interval, function=self.report)
        timer.daemon = True
        timer.start()

    def start(self):
        self.timestamp_last_input = self.get_timestamp()
        keyboard.on_release(callback=self.callback_keyboard)
        mouse.on_right_click(callback=self.callback_mouse_right)
        mouse.on_middle_click(callback=self.callback_mouse_middle)
        mouse.on_click(callback=self.callback_mouse_left)

        self.report()
        print(f"{format(self.get_timestamp(), '.3f')} - peripheral key statistics are running")
        keyboard.wait()
        mouse.unhook_all()

    def write_json_to_file(self, path: str, data: dict):
        with open(path, "w") as f:
            f.write(json.dumps(data, indent=4, sort_keys=True))
        print(f"{format(self.get_timestamp(), '.3f')} - wrote file to {path}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="PyPeripheralAnalytics",
        description="counts all the keys and mouse buttons pressed and accumulates the seconds between "
                    "the inputs to later calculate the average time that's needed to reach the key "
                    "according to how often it's needed."
    )
    parser.add_argument("filepath", help="path to a directory where the JSON file is saved")
    parser.add_argument("-i", "--interval", help="time (in seconds) till the report is saved to file", type=int)
    parser.add_argument("-b", "--break-time", help="maximum time the user needs between inputs", type=int)
    args = parser.parse_args()

    print(
        f"detected the following args:\n"
        f"   filepath: {args.filepath}\n"
        f"   interval: {args.interval}\n"
        f"   break_time: {args.break_time}"
    )

    if not args.interval and not args.break_time:
        peripheral_analytics = PeripheralAnalytics(path=args.filepath)

    elif not args.interval:
        peripheral_analytics = PeripheralAnalytics(path=args.filepath, break_time=args.break_time)

    elif not args.break_time:
        peripheral_analytics = PeripheralAnalytics(path=args.filepath, interval=args.interval)

    else:
        peripheral_analytics = PeripheralAnalytics(path=args.filepath, break_time=args.break_time, interval=args.interval)

    peripheral_analytics.start()
